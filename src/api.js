/* global require, module */
const ApiBuilder = require('claudia-api-builder');
const classes = require('./classes');

const api = new ApiBuilder();
api.corsOrigin(request => {
  const { origin } = request.normalizedHeaders;
  if (process.env.AllowedOrigins.split(' ').includes(origin)) {
    return origin;
  }
  return '';
});
api.corsHeaders('Accept,Authorization,Content-Type,Origin,Referer,User-Agent,X-Amz-Date,X-Api-Key,X-Amz-Security-Token');

api.registerAuthorizer('Authorizer', {
  lambdaName: 'Authorizer',
  validationExpression: 'Bearer .+',
  resultTtl: 3600,
});

// Classes Endpoints
const customAuthorizer = 'Authorizer';
api.get('/classes', classes.getAllHandler, { customAuthorizer });
api.post('/classes', classes.postHandler, { customAuthorizer });
api.get('/classes/{classId}', classes.getHandler, { customAuthorizer });
api.put('/classes/{classId}', classes.putHandler, { customAuthorizer });
api.delete('/classes/{classId}', classes.deleteHandler, { customAuthorizer });

module.exports = api;
