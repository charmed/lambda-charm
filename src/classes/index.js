const getHandler = require('./get');
const getAllHandler = require('./getAll');
const postHandler = require('./post');
const putHandler = require('./put');
const deleteHandler = require('./delete');

module.exports = {
  getHandler,
  getAllHandler,
  postHandler,
  putHandler,
  deleteHandler,
};
