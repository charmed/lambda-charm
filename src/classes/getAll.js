const AWS = require('aws-sdk');
const response = require('../../tools/response');
const log = require('../../tools/logger')('GET ALL CLASSES');

const { TableName } = process.env;

module.exports = async ({ context }) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const params = {
      TableName,
      KeyConditionExpression: 'userId = :userId',
      ExpressionAttributeNames: {
        '#name': 'name',
      },
      ExpressionAttributeValues: {
        ':userId': context.authorizerPrincipalId,
      },
      ProjectionExpression: 'classId, #name',
      Select: 'SPECIFIC_ATTRIBUTES',
    };
    log.info({ params });
    const result = await dynamodb.query(params).promise();
    const classList = (result.Items || []).map(({ classId, name }) => ({
      id: classId,
      name,
    }));
    log.success({ result, classList });
    return response.success.json(classList);
  }
  catch (error) {
    log.failure({ error });
    return response.error.unexpectedException(error);
  }
};
