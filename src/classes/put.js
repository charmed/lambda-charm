const AWS = require('aws-sdk');
const response = require('../../tools/response');
const log = require('../../tools/logger')('PUT CLASS');

const { TableName } = process.env;

// TODO - normalize or assert before putting in DB

module.exports = async ({ context, pathParams, body }) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const params = {
      TableName,
      Item: {
        userId: context.authorizerPrincipalId,
        classId: pathParams.classId,
        ...body,
      },
    };
    log.info({ params });
    const result = await dynamodb.put(params).promise();
    log.success({ result });
    return response.success.json(result);
  }
  catch (error) {
    log.failure({ error });
    return response.error.unexpectedException(error);
  }
};
