const AWS = require('aws-sdk');
const response = require('../../tools/response');
const log = require('../../tools/logger')('GET CLASS');

const { TableName } = process.env;

module.exports = async ({ context, pathParams }) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const params = {
      TableName,
      Key: {
        userId: context.authorizerPrincipalId,
        classId: pathParams.classId,
      },
    };
    log.info({ params });
    const result = await dynamodb.get(params).promise();
    const { userId, classId, ...retrievedClass } = result.Item || {};
    log.success({ result, retrievedClass });
    return response.success.json(retrievedClass);
  }
  catch (error) {
    log.failure({ error });
    return response.error.unexpectedException(error);
  }
};
