const AWS = require('aws-sdk');
const response = require('../../tools/response');
const log = require('../../tools/logger')('DELETE CLASS');

const { TableName } = process.env;

module.exports = async ({ context, pathParams }) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const params = {
      TableName,
      Key: {
        userId: context.authorizerPrincipalId,
        classId: pathParams.classId,
      },
    };
    log.info({ params });
    await dynamodb.delete(params).promise();
    log.success();
    return response.success.json();
  }
  catch (error) {
    log.failure({ error });
    return response.error.unexpectedException(error);
  }
};
