
This repo handles the creation/updates/deployment of the Charmed API.
It will create and configure API Gateway/Lambda as needed for the API.

You will need to manually attach the ddb read/write policies created by terraform to
the roles created by claudia.
