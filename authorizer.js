/* eslint-disable consistent-return */
const jwt = require('jsonwebtoken');
const got = require('got');

const getToken = ({ type, authorizationToken }) => {
  if (!type || type !== 'TOKEN') {
    throw new Error('Expected "event.type" parameter to have value "TOKEN"');
  }

  if (!authorizationToken) {
    throw new Error('Expected "event.authorizationToken" parameter to be set');
  }

  const match = authorizationToken.match(/^Bearer (.*)$/);
  if (!match || match.length < 2) {
    throw new Error(`Invalid Authorization token - ${authorizationToken} does not match "Bearer .*"`);
  }
  return match[1];
};

const getSigningKeys = async () => {
  const response = await got('https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com', { json: true });
  return response.body;
};

const getPolicyDocument = methodArn => {
  const [, regionId, accountId, apiId] = methodArn.match(/arn:aws:execute-api:([^:]+):([^:]+):([^/]+)/);
  return {
    Version: '2012-10-17',
    Statement: [{
      Action: 'execute-api:Invoke',
      Effect: 'Allow',
      // Allow all methods for the requested API
      Resource: `arn:aws:execute-api:${regionId}:${accountId}:${apiId}/*/*/*`,
    }],
  };
};

exports.handler = async (event, context) => {
  try {
    // Grab and decode token
    const token = getToken(event);
    const { header } = jwt.decode(token, { complete: true }) || {};

    // Check for the presence of a signing key ID
    if (!header || !header.kid) {
      throw new Error('Invalid token');
    }

    // Get Google Signing Keys
    const signingKeys = await getSigningKeys();
    if (!signingKeys) {
      throw new Error('Unable to retrieve Google Signing Keys');
    }

    // Grab signing key with matching ID from Google Signing Keys
    const signingKey = signingKeys[header.kid];
    if (!signingKey) {
      throw new Error('Token signed with unknown key');
    }

    // Verify token
    const verifiedToken = jwt.verify(token, signingKey, {
      algorithms: ['RS256'],
      audience: 'charmed-cloud',
      issuer: 'https://securetoken.google.com/charmed-cloud',
    });

    // Return policy document to allow invoking API
    return ({
      principalId: verifiedToken.sub,
      policyDocument: getPolicyDocument(event.methodArn),
    });
  }
  catch (err) {
    console.log('Unauthorized', err.message || JSON.stringify(err));
    context.fail('Unauthorized');
  }
};
