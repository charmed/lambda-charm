const { ApiResponse } = require('claudia-api-builder');

const headers = {
  json: { 'Content-Type': 'application/json' },
  text: { 'Content-Type': 'text/plain' },
};

module.exports = {
  success: {
    empty: () => new ApiResponse(undefined, {}, 200),
    text: body => new ApiResponse(body || '', headers.text, 200),
    json: body => new ApiResponse(body || {}, headers.json, 200),
  },
  error: {
    noSuchMethod: error => new ApiResponse({ error, errorMsg: 'No such method exists.' }, headers.json, 405),
    unexpectedException: error => new ApiResponse({ error, errorMsg: 'An unexpected error occured.' }, headers.json, 500),
  },
};
