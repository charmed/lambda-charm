module.exports = ctx => ({
  info: o => console.log(ctx, JSON.stringify(o || {})),
  success: o => console.log(`[${ctx}] -> SUCCESS`, JSON.stringify(o || {})),
  failure: o => console.log(`[${ctx}] -> FAILURE`, JSON.stringify(o || {})),
});
