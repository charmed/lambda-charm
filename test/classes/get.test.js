const AWS = require('aws-sdk-mock');
const get = require('../../src/classes/get');

const result = {
  Item: { yay: 'it worked' },
};

const sampleEvent = {
  context: {
    authorizerPrincipalId: 'heyooo',
  },
  pathParams: {
    classId: 'blah',
  },
};

describe('GET /classes/{classId}', () => {
  it('gets TableName from environment', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'get', async params => {
      expect(params.TableName).toEqual(process.env.TableName);
      return result;
    });
    await get(sampleEvent);
  });

  it('uses userId from context', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'get', async params => {
      expect(params.ExpressionAttributeValues[':userId']).toEqual(sampleEvent.context.authorizerPrincipalId);
      return result;
    });
    await get(sampleEvent);
  });

  it('return 200 on success', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'get', async () => result);
    const { code } = await get(sampleEvent);
    expect(code).toEqual(200);
  });

  it('return class as response body', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'get', async () => result);
    const { response } = await get(sampleEvent);
    console.error('yay!');
    expect(response).toEqual(result.Item);
  });

  afterEach(() => {
    AWS.restore('DynamoDB.DocumentClient', 'get');
  });
});
