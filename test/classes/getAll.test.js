const AWS = require('aws-sdk-mock');
const getAll = require('../../src/classes/getAll');

const responses = {
  EMPTY: {
    Items: [],
  },
  SOME: {
    Items: [
      { classId: 1, name: 'class1' },
      { classId: 2, name: 'class2' },
      { classId: 3, name: 'class3' },
    ],
  },
};

const sampleEvent = {
  context: {
    authorizerPrincipalId: 'heyooo',
  },
};

describe('GET /classes', () => {
  it('gets TableName from environment', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'query', async params => {
      expect(params.TableName).toEqual(process.env.TableName);
      return responses.EMPTY;
    });
    await getAll(sampleEvent);
  });

  it('gets userId from context', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'query', async params => {
      expect(params.ExpressionAttributeValues[':userId']).toEqual(sampleEvent.context.authorizerPrincipalId);
      return responses.EMPTY;
    });
    await getAll(sampleEvent);
  });

  it('return 200 on success', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'query', async () => responses.EMPTY);
    const { code } = await getAll(sampleEvent);
    expect(code).toEqual(200);
  });

  it('return [empty] Items as response body', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'query', async () => responses.EMPTY);
    const { response } = await getAll(sampleEvent);
    expect(response).toEqual(responses.EMPTY.Items);
  });

  it('return Items as response body', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'query', async () => responses.SOME);
    const { response } = await getAll(sampleEvent);
    const expected = responses.SOME.Items.map(({ classId, name }) => ({
      id: classId,
      name,
    }));
    expect(response).toEqual(expected);
  });

  afterEach(() => {
    AWS.restore('DynamoDB.DocumentClient', 'query');
  });
});
