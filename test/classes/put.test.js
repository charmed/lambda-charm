const AWS = require('aws-sdk-mock');
const put = require('../../src/classes/put');

const sampleEvent = {
  context: {
    authorizerPrincipalId: 'heyooo',
  },
  pathParams: {
    classId: 'ajw490jawca4',
  },
  body: {
    classId: 'dont use this id if it exists',
    name: 'Class Name',
    blah: 'bloo',
  },
};

describe('PUT /classes/{classId}', () => {
  it('uses TableName from environment', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'put', async params => {
      expect(params.TableName).toEqual(process.env.TableName);
    });
    await put(sampleEvent);
  });

  it('uses userId from context, classId from pathParams', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'put', async params => {
      expect(params.Item.userId).toEqual(sampleEvent.context.authorizerPrincipalId);
      expect(params.Item.classId).toEqual(sampleEvent.pathParams.classId);
    });
    await put(sampleEvent);
  });

  it('uses rest of data from request body', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'put', async params => {
      expect(params.Item.name).toEqual(sampleEvent.body.name);
      expect(params.Item.blah).toEqual(sampleEvent.body.blah);
    });
    await put(sampleEvent);
  });

  it('return 200 on success', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'put', async () => {
    });
    const { code } = await put(sampleEvent);
    expect(code).toEqual(200);
  });

  it('return result as response-body', async () => {
    const expected = { a: 1 };
    AWS.mock('DynamoDB.DocumentClient', 'put', async () => expected);
    const { response } = await put(sampleEvent);
    expect(response).toEqual(expected);
  });

  afterEach(() => {
    AWS.restore('DynamoDB.DocumentClient', 'put');
  });
});
