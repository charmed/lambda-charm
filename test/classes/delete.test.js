const AWS = require('aws-sdk-mock');
const deleteHandler = require('../../src/classes/delete');

const sampleEvent = {
  context: {
    authorizerPrincipalId: 'heyooo',
  },
  pathParams: {
    classId: 'ajw490jawca4',
  },
};

describe('DELETE /classes/{classId}', () => {
  it('gets TableName from environment', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'delete', async params => {
      expect(params.TableName).toEqual(process.env.TableName);
    });
    await deleteHandler(sampleEvent);
  });

  it('uses userId from context, classId from pathParams', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'delete', async params => {
      expect(params.Key.userId).toEqual(sampleEvent.context.authorizerPrincipalId);
      expect(params.Key.classId).toEqual(sampleEvent.pathParams.classId);
    });
    await deleteHandler(sampleEvent);
  });

  it('return 200 on success', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'delete', async () => {
    });
    const response = await deleteHandler(sampleEvent);
    console.log(response);
    expect(response.code).toEqual(200);
  });

  afterEach(() => {
    AWS.restore('DynamoDB.DocumentClient', 'delete');
  });
});
