const AWS = require('aws-sdk-mock');
const post = require('../../src/classes/post');

const sampleEvent = {
  context: {
    authorizerPrincipalId: 'heyooo',
  },
  body: {
    classId: 'ajw490jawca4',
    name: 'Class Name',
    blah: 'bloo',
  },
};

describe('POST /classes', () => {
  it('gets TableName from environment', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'put', async params => {
      expect(params.TableName).toEqual(process.env.TableName);
    });
    await post(sampleEvent);
  });

  it('uses userId from context, classId as id from request body', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'put', async params => {
      expect(params.Item.userId).toEqual(sampleEvent.context.authorizerPrincipalId);
      expect(params.Item.classId).toEqual(sampleEvent.body.id);
    });
    await post(sampleEvent);
  });

  it('uses rest of data from request body', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'put', async params => {
      expect(params.Item.name).toEqual(sampleEvent.body.name);
      expect(params.Item.blah).toEqual(sampleEvent.body.blah);
    });
    await post(sampleEvent);
  });

  it('return 200 on success', async () => {
    AWS.mock('DynamoDB.DocumentClient', 'put', async () => {
    });
    const { code } = await post(sampleEvent);
    expect(code).toEqual(200);
  });

  it('return result as response-body', async () => {
    const expected = { a: 1 };
    AWS.mock('DynamoDB.DocumentClient', 'put', async () => expected);
    const x = await post(sampleEvent);
    const { response } = x;
    expect(response).toEqual(expected);
  });

  afterEach(() => {
    AWS.restore('DynamoDB.DocumentClient', 'put');
  });
});
